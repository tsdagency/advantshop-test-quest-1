"use strict";
/*
  App base path var
*/
const $bp = __dirname;

/*
  Deps
*/
const gulp = require('gulp'),
      path = require('path'),
      browserSync = require('browser-sync'),
      njk = require('gulp-nunjucks-render'),
      sass = require('gulp-sass'),
      postcss = require('gulp-postcss'),
      cssgrace = require('cssgrace'),
      mqpacker = require('css-mqpacker'),
      cssnano = require('cssnano'),
      urlrewrite = require('postcss-urlrewrite'),
      prettify = require('gulp-html-prettify'),
      usemin = require('gulp-usemin'),
      uglify = require('gulp-uglify'),
      htmlMin = require('gulp-htmlmin'),
      autoprefixer = require('autoprefixer'),
      bowerAssets = require('gulp-bower-assets'),
      gFile = require('gulp-file'),
      image = require('gulp-image'),
      watch = require('gulp-chokidar'),
      gPlumber = require('gulp-plumber'),
      webpack = require('webpack'),
      gulpWebpack = require('gulp-webpack'),
      bowerWebpackPlugin = require('bower-webpack-plugin'),
      rename = require('gulp-rename'),
      copy = require('gulp-copy');

/* PRECONFIG */
const IS_DEVELOPMENT_MODE = (!process.env.NODE_ENV || process.env.NODE_ENV.toLowerCase() !== 'production'),
      BUILD_MODE = IS_DEVELOPMENT_MODE ? 'DEV' : 'PRODUCTION',
      IS_USING_BS = (!process.env.BUILD_NO_BS || process.env.BUILD_NO_BS !== 'true') ? true : false,
      WATCH_ENABLED = (!process.env.WATCH_ENABLED || process.env.WATCH_ENABLED !== 'false') ? true : false;

// path variable
const $p = {
  conf : {
    sass : path.join($bp, 'conf/sass.conf.json'),
    autoprefixer : path.join($bp, 'conf/autoprefixer.conf.json'),
    bowerAssets : path.join($bp, 'conf/bower_assets.conf.json'),
    babel : path.join($bp, 'conf/babel.conf.json'),
    webpack : path.join($bp, 'conf/webpack.conf.json'),
    njk_data : path.join($bp, 'conf/njk_data.conf.json')
  },
  build : {
    out : path.join($bp, 'build')
  },
  njk : {
    src : path.join($bp, 'njk/*.njk'),
    out : path.join($bp, 'build'),
    watch : path.join($bp, 'njk/**/*.njk')
  },
  sass: {
    src : [path.join($bp, 'sass/**/*.sass'),path.join($bp, 'sass/**/*.scss')],
    out : path.join($bp, 'build/css'),
    http_path : '/aisofjoaisjf'
  },
  js : {
    src : path.join($bp, 'js/**/*.js'),
    out : path.join($bp, 'build/js/')
  },
  php : {
    src : path.join($bp, 'php/**/*.php'),
    out : path.join($bp, 'build/php')
  },
  fonts : {
    src : path.join($bp, 'fonts/**/*'),
    out : path.join($bp, 'build/fonts')
  },
  img : {
    src : [
      path.join($bp, 'img/**/*.jpg'),
      path.join($bp, 'img/**/*.gif'),
      path.join($bp, 'img/**/*.png'),
      path.join($bp, 'img/**/*.svg'),
      path.join($bp, 'img/**/*.ico')
    ],
    out : path.join($bp, 'build/')
  }
}

// module configs
const $conf = {
  preprocess : {
    tokens : {
      '${basePath}' : $bp
    }
  },
  sass : {
    includePaths : []
  },
  autoprefixer : {},
  bowerAssets : {},
  webpack : {
    externals: [
      {
        "window": "window"
      }
    ],
    plugins: [
      new bowerWebpackPlugin({
        includes : /\.js$/
      }),
      new webpack.ProvidePlugin({
        jQuery: 'jquery',
        $: 'jquery',
        jquery: 'jquery',
        "window.jQuery" : 'jquery'
      })
    ]
  }
}

let $postcssProcessors = [ ];

let postcssRewriteConfig = {
  properties : ['background', 'content'],
  rules : [
    {
      from : /\.\.\/img/,
      to   : 'img'
    }
  ]
}

const reloadConfig = require('./conf/conf.js')($p, $conf);

const plumber = () => {
  return gPlumber({
    handleError : (err) => {
      console.error(err);
    }
  });
}

const initBS = () => {
  let bsSettings = {
    host : 'localhost',
    port : process.env.PORT || 9000,
    logPrefix : 'ADVSHP',
    open : false
  }

  if(process.env.PROXY) {
    bsSettings['proxy'] = process.env.PROXY;
    bsSettings['open'] = 'external';
  }
  else {
    bsSettings['server'] = {
      baseDir : $p.build.out
    }
  }

  if (!IS_USING_BS) {
    bsSettings['server'] = false;
  }
  else {
    bs.init(bsSettings);
  }
}

// init bs:
let bs = browserSync.create();
let reload = bs.reload;

let processImage = (file) => {
  return gulp.src(file, {
    base : '.'
  })
    .pipe(plumber())
    .pipe(image({
      pngquant: true,
      optipng: false,
      zopflipng: true,
      advpng: true,
      jpegRecompress: false,
      jpegoptim: true,
      mozjpeg: true,
      gifsicle: true,
      svgo: true
    }))
    .pipe(gulp.dest($p.img.out))
    .pipe(reload({
      stream : true
    }));
}

/*
  BUILD INFO
*/
console.log(`BUILDER CONFIG:`)
console.log(`[BUILD MODE]: ${BUILD_MODE}`)
console.log(`[BROWSER SYNC]: ${IS_USING_BS}`);
console.log(`[WATCH FILES]: ${WATCH_ENABLED}`)
console.log('');

// initialize configs
reloadConfig();

if( ! IS_DEVELOPMENT_MODE ) {
  $postcssProcessors.push(cssnano({
    autoprefixer: false
  }));
  $postcssProcessors.push(cssgrace);
  $postcssProcessors.push(urlrewrite(postcssRewriteConfig));
  $postcssProcessors.push(mqpacker());
  $postcssProcessors.push(autoprefixer($conf.autoprefixer));
}

/*
  TASKS
*/

/*
  Copy bower assets due to config
*/
gulp.task('bower-assets', () => {
  return gFile('bower-assets-compiler.tmp', JSON.stringify($conf.bowerAssets), { src: true })
    .pipe(plumber())
    .pipe(bowerAssets({
      prefix: false
    }))
    .pipe(gulp.dest($p.build.out));
});

/*
  Build javascript
*/
gulp.task('build:js', () => {

  bs.notify("Компиляция Javascript...");

  return gulp.src('js/*.js') //$p.js.src)
    .pipe(gulpWebpack($conf.webpack))
    .pipe(gulp.dest($p.js.out))
    .pipe(reload({ stream : true }))
});

/*
  Build css
*/
gulp.task('build:css', (done) => {

  bs.notify("Компиляция SASS...");

  return gulp.src($p.sass.src)
    .pipe(gPlumber())
    .pipe(sass.sync($conf.sass).on('error', done))
    .pipe(postcss($postcssProcessors))
    .pipe(gulp.dest($p.sass.out))
    .pipe(reload({ stream : true }))
});

/*
  Build nunjucks
*/
gulp.task('build:njk', (done) => {
  if (!IS_DEVELOPMENT_MODE) {
    console.log('BUILD_HTML')
    return buildProd(done);
  }

  bs.notify("Компиляция nunjucks...");

  return gulp.src($p.njk.src)
    .pipe(gPlumber())
    .pipe(njk({
      path : path.join($bp, 'njk'),
      data : {
        DEBUG : IS_DEVELOPMENT_MODE,
        DATA : $conf.njk_data
      }
    }))
    .pipe(gulp.dest($p.njk.out))
    .pipe(reload({ stream : true }));
});

/*
  Process images
*/
gulp.task('build:img', function() {
  return processImage($p.img.src);
});

/*
  Process php
*/
gulp.task('build:php', function() {
  return gulp.src($p.php.src)
    .pipe(gulp.dest($p.php.out));
    // .pipe(gulp.dest($p.php.out));
});

/*
  Copy fonts
*/
gulp.task('build:fonts', function() {
  return gulp.src($p.fonts.src)
    .pipe(gulp.dest($p.fonts.out));
});

/*
  Process favicon
*/
gulp.task('build:favicon', function() {

});

// продакшн режим - всякие сжималки, инлайнеры и прочее
function buildProd(done) {

  bs.notify("Идет перекомпиляция, ждите перезагрузки...");

  return gulp.src($p.njk.src)
    .pipe(gPlumber())
    .pipe(njk({
      path : path.join($bp, 'njk'),
      data : {
        DEBUG : IS_DEVELOPMENT_MODE,
        USEMIN : true,
        DATA : $conf.njk_data
      }
    }))
    .pipe(usemin({
      path : $bp,
      css : [
        sass.sync($conf.sass).on('error', done),
        postcss($postcssProcessors),
        'concat'
      ],
      inlinecss : [
        sass.sync($conf.sass).on('error', done),
        postcss($postcssProcessors),
        'concat'
      ],
      js : [
        gulpWebpack($conf.webpack),
        'concat'
      ],
      inlinejs : [
        gulpWebpack($conf.webpack),
        uglify(),
        'concat'
      ],
      html : [
        () => {
          if( ! IS_DEVELOPMENT_MODE ) {
            return htmlMin({
              collapseWhitespace : true
            });
          }
          return prettify();
        }
      ]
    }))
    .on('error', done)
    .pipe(gulp.dest($p.njk.out))
    .pipe(reload({ stream : true }));
}

/*
  Setup watchers
*/
gulp.task('watch', () => {

  if (!WATCH_ENABLED) {
    return;
  }

  watch($p.njk.watch, () => {
    gulp.start('build:njk');
  });

  watch($p.sass.src, () => {
    if( ! IS_DEVELOPMENT_MODE ) {
      gulp.start('build:njk');
    }
    else {
      gulp.start('build:css');
    }
  });

  watch($p.js.src, () => {
    if( ! IS_DEVELOPMENT_MODE ) {
      gulp.start('build:njk');
    }
    else {
      gulp.start('build:js');
    }
  });

  watch($p.fonts.src, () => {
    gulp.start('build:fonts');
  });

  /*
    Watch for configs changes
  */
  for (let key in $p.conf) {
    watch($p.conf[key], () => {
      console.log(`Reloading config from ${key}`);
      reloadConfig(key);
    });
  }

  watch($p.conf.bowerAssets, () => {
    gulp.start('bower-assets');
  });

  watch($p.php.src, () => {
    gulp.start('build:php');
  });

  watch($p.img.src, () => {})
    .on('add', processImage)
    .on('change', processImage);
});

/*
  Fast start - only watchers
*/
gulp.task('fast', ['watch'], () => {
  initBS();
});

/*
  Default start - compile and watch
*/
gulp.task('default', ['build:css', 'build:js', 'build:fonts', 'build:njk', 'build:php', 'watch'], () => {
  initBS();
});