module.exports = function($p, $conf) {
	"use strict";

	const _extend = require('lodash/extend');

	const fs = require('fs');
	const sectionCallbacks = {}

	const defaultCallback = (path) => {
		let sassConf = fs.readFileSync(path);
		console.log(path);
		try {
			sassConf = preprocess(sassConf);
			return JSON.parse(sassConf);
		}
		catch (e) {
			console.error(e);
		}

		return {};
	}

	const preprocess = (content) => {
		let key;

		for(key in $conf.preprocess.tokens) {
			if($conf.preprocess.tokens.hasOwnProperty(key)) {
				let val = $conf.preprocess.tokens[key];
				content = content.toString().split(key).join(val);
			}
		}

		return content;
	}


	// config reloader
	let reloadConfig = (section) => {
		if(section) {
			$conf[section] = _extend($conf[section], defaultCallback($p.conf[section]));
		}
		else {
			for(section in $p.conf) {
				reloadConfig(section);
			}
		}
		console.log(`config reloaded for section ${section}`);
	}

	return reloadConfig;
}